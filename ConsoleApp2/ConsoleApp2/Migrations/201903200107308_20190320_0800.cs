namespace ConsoleApp2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _20190320_0800 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArticleCommonTables",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ArticleTitle = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleFirstTypeTables", t => t.Id)
                .ForeignKey("dbo.ArticleSecondTypeTables", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.ArticleFirstTypeTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArticleFirstTypeProperty = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ArticleSecondTypeTables",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ArticleSecondTypeProperty = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ArticleCommonTables", "Id", "dbo.ArticleSecondTypeTables");
            DropForeignKey("dbo.ArticleCommonTables", "Id", "dbo.ArticleFirstTypeTables");
            DropIndex("dbo.ArticleCommonTables", new[] { "Id" });
            DropTable("dbo.ArticleSecondTypeTables");
            DropTable("dbo.ArticleFirstTypeTables");
            DropTable("dbo.ArticleCommonTables");
        }
    }
}
