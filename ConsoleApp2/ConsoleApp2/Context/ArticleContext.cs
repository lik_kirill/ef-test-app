﻿using ConsoleApp2.Models;
using System.Data.Entity;

namespace ConsoleApp2.Context
{
    class ArticleContext: DbContext
    {
        public ArticleContext()
            : base("DbConnection")
        { }

        public DbSet<ArticleCommonTable> CommonTable { get; set; }
    }
}
