﻿using System.ComponentModel.DataAnnotations;

namespace ConsoleApp2.Models
{
    class ArticleSecondTypeTable
    {
        public int Id { get; set; }
        public string ArticleSecondTypeProperty { get; set; }
    }
}
