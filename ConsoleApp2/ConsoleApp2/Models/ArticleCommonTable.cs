﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ConsoleApp2.Models
{
    class ArticleCommonTable
    {
        [Key]
        public int Id { get; set; }
        public string ArticleTitle { get; set; }
        public int Type { get; set; }
        [ForeignKey("Id")]
        public ArticleFirstTypeTable ArticleFirstTypeTable { get; set; }
        [ForeignKey("Id")]
        public ArticleSecondTypeTable ArticleSecondTypeTable { get; set; }
    }
}
