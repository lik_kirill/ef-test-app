﻿using System.ComponentModel.DataAnnotations;

namespace ConsoleApp2.Models
{
    class ArticleFirstTypeTable
    {
        public int Id { get; set; }
        public string ArticleFirstTypeProperty { get; set; }
    }
}
